import pyb
from pyb import Pin, Timer


def setupChannels(f):
	pinB0 = Pin.cpu.B0
	pinB1 = Pin.cpu.B1
	pinB4 = Pin.cpu.B4
	pinB5 = Pin.cpu.B5
	
	tim3 = Timer(3, freq=f)
	ch1 = tim3.channel(1, Timer.PWM, pin = pinB0)
	ch2 = tim3.channel(2, Timer.PWM, pin = pinB1)
	ch3 = tim3.channel(3, Timer.PWM, pin = pinB4)
	ch4 = tim3.channel(4, Timer.PWM, pin = pinB5)
	
	channels = [ch1, ch2, ch3, ch4]
	
	for i in range(len(channels)):
		if(i%2):
			channels[i].pulse_width_percent(75)
		else:
			channels[i].pulse_width_percent(0)

