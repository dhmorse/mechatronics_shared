# -*- coding: utf-8 -*-
"""
Created on Sun Mar  7 13:19:27 2021

@author: jacob
"""

import MotorDriver
from MotorDriver import MotorDriver
import pyb
import EncoderDriver
from EncoderDriver import EncoderDriver

faultflag = 0 #flag to be triggered when a fault occurs
fault = pyb.Pin.board.PB2 #fault pin on the nucleo
def fault_isr (fault): #fault isr to be triggered when bad juju happens
    global faultflag #use fault flag in here
    faultflag = 1 #set flag to 1 to indicate a fault has occured    
extint = pyb.ExtInt(pyb.Pin.board.PB2, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, fault_isr)
#external interrup to be used when current draw to PB2 is too high

fix = pyb.Pin.board.PC13 #"fix" pin, all this stuff isn't working though
def fix_isr (fix): #not working
    global faultflag #not working
    faultflag = 0 #not working    
fixtint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, fix_isr)
#not working

var = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB4, pyb.Pin.board.PB5, 3, 1, 2)
var2 = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB0, pyb.Pin.board.PB1, 3, 3, 4)
#create MotorDriver object
balancer = EncoderDriver(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 1, 2, pyb.Timer(4, prescaler = 7, period = 0x7fffffff))
balancer.setPosition(0)
balancer2 = EncoderDriver(pyb.Pin.board.PC6, pyb.Pin.board.PC7, 1, 2, pyb.Timer(8, prescaler = 7, period = 0x7fffffff))
balancer2.setPosition(0)
state = 0 #use state indicator in loop later
faultflag = 0 #start out making sure fault flag is zero
start = 300 #enabling the motor caused a fault in my system, so I created this
#to let the loop below run 300 so it can at least get started
adc = pyb.ADC(pyb.Pin.board.A0) #create ADC object so that pin A0 can read the
#value coming from the blue button on the board
duty = 0
duty2 = 0
var.set_duty(0)
var2.set_duty(0)
var.enable()
dut = 75
balanceval = 0
balanceval2 = 0
while True: #loop!
    if start > 0: #if motor is just starting, set faultflag to zero to allow it
    #time to get up to speed
        faultflag = 0 #set flag to zero to ignore the weird glitch
        if start == 300:
            var.enable()
            var2.enable()
            start = start - 1
            print('high')
        
        elif start == 0: #once start reaches 0, keep it there
            start = 0 #keep start equal to 0 so it doesn't try running back through
        else: #if it hasn't given the motors a chance
            start = start - 1 #decrement the start counter
    else: #if start is equal to zero, now allow the fault flag to affect the system
        pass
    if faultflag == 0: #if fault flag is not on, run normally
        if state == 0: # if motor is just starting
            var.enable() #enable the motor
            balanceval = balancer.balanceDif()
            if -32768 < balanceval < -10:
                duty = (((balanceval/32768)*40)-20)
                duty = -dut
                var.set_duty(duty)
            elif -10< balanceval < 10:
                duty = 0
                var.stop()
            else: 
                #if balanceval < 0:
                duty = ((((65536+balanceval)/32767)*40)+20)
                var.set_duty(dut)
                #else:
                    #duty = (((balanceval/32767)*40)+50)    
            #var.set_duty(duty) #set the duty cycle to 50%
            balanceval2 = balancer2.balanceDif()
            if -32768 < balanceval2 < -10:
                duty2 = (((balanceval2/32768)*40)-20)
                var2.set_duty(-dut)
            elif -10< balanceval2 < 10:
                duty2 = 0
                var2.stop()
            else: 
                #if balanceval2 < 0:
                duty2 = ((((65536+balanceval2)/32767)*40)+20)
                var2.set_duty(dut)
               # else:
                #    duty2 = (((balanceval2/32767)*40)+50) 
            #var2.set_duty(duty2) #set the duty cycle to 50%
            state = 0 #set the state to 1 so it doesnt keep re-enabling the motor
        if state > 0: #not just starting anymore
            pass #run
            
    else: #if fault flag has been set come here
        var.stop() #stop the motors
        var2.stop() #stop the motors
        var.disable() #disable the motors
        state = 0 #set the state back to zero so that when it starts again,
        #we re-enable the motors and reset the duty cycle
        if(adc.read() < 3000): #if the blue button has been pressed
            faultflag = 0 #allow the motors to run again
            start = 300 #set the start back to 300 to give the motors time again
        else:
            pass #otherwise, keep not running
        
    