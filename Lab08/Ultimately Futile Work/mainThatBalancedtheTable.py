# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 16:38:52 2021

@author: jacob
"""

import MotorDriver
from MotorDriver import MotorDriver
import pyb
import EncoderDriver
from EncoderDriver import EncoderDriver
import touchy2
from touchy2 import touchy
var = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB4, pyb.Pin.board.PB5, 3, 1, 2)
var2 = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB0, pyb.Pin.board.PB1, 3, 3, 4)
balancer = EncoderDriver(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 1, 2, pyb.Timer(4, prescaler = 7, period = 0xff))
beginner = 0
balancer.setPosition(beginner)
balancer2 = EncoderDriver(pyb.Pin.board.PC6, pyb.Pin.board.PC7, 1, 2, pyb.Timer(8, prescaler = 7, period = 0xff))
balancer2.setPosition(beginner)
fault = pyb.Pin.board.PB2 #fault pin on the nucleo
def fault_isr (fault): #fault isr to be triggered when bad juju happens
    global beginner #use fault flag in here
    beginner = beginner - balancer2.getPosition() #set flag to 1 to indicate a fault has occured
    balancer2.setPosition(beginner)     
extint = pyb.ExtInt(pyb.Pin.board.PB2, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, fault_isr)
#external interrup to be used when current draw to PB2 is too high
var.enable()
while True: 
   # var.enable()
   # balancer.setPosition(beginner)
   # balancer2.setPosition(beginner)
    if -128 < balancer.balanceDif() <= -40:
        var.set_duty(-80)
    elif -40 < balancer.balanceDif() <= -20:
        var.set_duty(-80)
    elif -20 < balancer.balanceDif() <= -5:
        var.set_duty(-50)
    elif -5 < balancer.balanceDif() < -1:
        var.set_duty(-30)    
    elif -1 <= balancer.balanceDif() :
        var.stop()
    if -215 < balancer.balanceDif() <= -128:
        var.set_duty(90)
    elif -235 < balancer.balanceDif() <= -215:
        var.set_duty(99)
    elif -250 < balancer.balanceDif() <= -235:
        var.set_duty(90)
    elif -256 < balancer.balanceDif() <= -250:
        var.set_duty(80)
        
    if -128 < balancer2.balanceDif() <= -40:
        var2.set_duty(-80)
    elif -40 < balancer2.balanceDif() <= -20:
        var2.set_duty(-80)
    elif -20 < balancer2.balanceDif() <= -5:
        var2.set_duty(-50)
    elif -5 < balancer2.balanceDif() < -1:
        var2.set_duty(-30)    
    elif -1 <= balancer2.balanceDif() :
        var2.stop()
    if -215 < balancer2.balanceDif() <= -128:
        var2.set_duty(90)
    elif -235 < balancer2.balanceDif() <= -215:
        var2.set_duty(99)
    elif -250 < balancer2.balanceDif() <= -235:
        var2.set_duty(90)
    elif -256 < balancer2.balanceDif() <= -250:
        var2.set_duty(70)
    print(balancer2.getPosition())