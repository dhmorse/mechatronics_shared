# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 17:33:02 2021

@author: jacob
"""

import pyb
from pyb import Pin, Timer, ExtInt

import utime

#import micropython

class EncoderDriver:
    """
    """

    def __init__(self, pin1, pin2, ch1, ch2, timer):
        """
        @param pin1 pin to read encoder chA values
        @param pin2 pin to read encoder chB values
        @param ch1 encoder channel A
        @param ch2 endcoder channel B
        @param timer timer to read encoder
        """

        self.pin1 = pin1
        self.pin2 = pin2
        self.timer = timer
        self.timer.channel(ch1, Timer.ENC_A, pin = self.pin1)
        self.timer.channel(ch2, Timer.ENC_A, pin = self.pin2)
		
        self.prevPosition = (0, 0)
        self.position = (0, 0)

    def update(self):	
        """
        Update the position of the encoder
        """
        self.prevPosition = self.position
        self.position = self.getPosition()


    def getPosition(self):
        """
        """
        return(self.timer.counter())
    
    def setPosition(self, value):
        self.value = value

    def getDelta(self):
        """
        """
        return(self.prevPosition - self.position)
    
    def balanceDif(self):
        return(self.value - self.getPosition())