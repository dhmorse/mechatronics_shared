# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 20:56:55 2021

@author: jacob
"""

import MotorDriver
from MotorDriver import MotorDriver
import pyb
import EncoderDriver
from EncoderDriver import EncoderDriver
import touchy2
from touchy2 import touchy
var = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB4, pyb.Pin.board.PB5, 3, 1, 2)
#controls the y
var2 = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB0, pyb.Pin.board.PB1, 3, 3, 4)
#controls the x
touch = touchy(pyb.Pin.board.PA1, pyb.Pin.board.PA7, pyb.Pin.board.PA0, pyb.Pin.board.PA6, 400, 200, 270, 100)

lengthpast = 0
widthpast = 0
yduty = 0
xduty = 0
var.enable()
while True:
    spot = touch.allread()
    onoff = spot[2]
    if onoff == 1:
        length = -spot[0]
        lspeed = lengthpast-length
        lengthpast = length
        width = -spot[1]
        wspeed = widthpast-width
        widthpast = width
        xduty = (2.15*length*80/200) + (7.55*lspeed*20/200)
       # yduty = (2.15*width*50/100) + (7.55*wspeed*50/100)
        var2.set_duty(xduty)
        var.set_duty(yduty)
        print(spot)
    else:
        var.stop()
        var2.stop()