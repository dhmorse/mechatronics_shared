# -*- coding: utf-8 -*-
"""@file EncoderDriver.py
@brief Shows where the motor is
@details This class creates an interface between the Nucleo and the motors. The
         Encoder has a spoked wheel inside of it that spin with the same 
         angular velocity as the motor. It shines a light through that wheel 
         and two sensors "observe" that light shining through. These two
         sensors are out of phase by 90 degrees, which means that when we 
         xor the outputs of both sensors on the same timeline, we find the 
         speed of the spoked wheel and therefore the speed of the motor. This
         class allows the user to create an object that can be used to read 
         that position of the encoder (and therefore the motor). The individual
         reading is only useful relative to the particular angle the motor was
         at when it turned on, as that is the set zero point, but collectively
         multiple readings can be used to tell the speed accurately.
@author Jacob Everest
@author Hunter Morse
@date Mar. 9, 2021
@copyright Hunter might be fine with this word
"""

import pyb #import the pyboard class
from pyb import Pin, Timer, ExtInt #from the pyboard class file, import the
#pin, timer, and extint methods
import utime #import the utime class

class EncoderDriver:
    """
    @brief Tells where the motor is at any given time
    @details Input 2 pins, 2 channels, and a timer object to run. Once given,
             this object will be able to read the motor position, update its 
             position, set a desired position, compute its change in position,
             and compute how far it is from the users desired position. These
             position readings can be used to determine where the motor is at 
             any given time relative to where it started as well as the motor 
             speed.
    @autor Hunter Morse
    @author Jacob Everest
    @date Mar. 9, 2021
    @copyright 
    """

    def __init__(self, pin1, pin2, ch1, ch2, timer):
        """
        @brief Initialization Function
        @details Stores the parameters input by the user when creating the 
                 class object so that they may be used later by other 
                 functions.
        @param pin1 pin to read encoder chA values
        @param pin2 pin to read encoder chB values
        @param ch1 encoder channel A
        @param ch2 endcoder channel B
        @param timer timer to read encoder
        @author Hunter Morse
        @author Jacob Everest
        @date Mar. 9, 2021
        """
        ##@brief pyb.Pin object 
        # @details Identifies the pin to which channel A from the encoder 
        #          is connected.
        self.pin1 = pin1
        
        ##@brief pyb.Pin object 
        # @details Identifies the pin to which channel B from the encoder 
        #          is connected.
        self.pin2 = pin2
        
        ##@brief Timer object
        # @details Timer object which we set up with the timer number, 
        #          desired prescaler value, and period length.
        self.timer = timer
        
        self.timer.channel(ch1, Timer.ENC_A, pin = self.pin1) #initialize channel object
        self.timer.channel(ch2, Timer.ENC_A, pin = self.pin2) #initialize channel object
        
		##@brief Previous position value
        # @details Variable which we will use to save the previous location
        #          reading from the encoder. Set channel A and Channel B to
        #          zero upon initialization.
        self.prevPosition = (0, 0)
        
        ##@brief Position value
        # @details Variable which we will use to store the current location 
        #          reading from the encoder. Set channel A and Channel B to
        #          zero upon initialization.
        self.position = (0, 0)

    def update(self):	
        """
        @brief Update method
        @details Updates the position of the motor so that the user can later
                 tell how far the motor has traveled. It stores the current
                 value stored in the position variable as "prevPosition", then 
                 runs the getPosision() method that will be described later to 
                 put the current value for the position into the position 
                 variable. This way the most recent position value and the one
                 before it are both stored somewhere. Does not return a value.
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        """
        self.prevPosition = self.position #save the current value as the 
        #previous value to make room for a new value
        self.position = self.getPosition() #replace previous value with a new
        #value


    def getPosition(self):
        """
        @brief Method to get the position
        @details This method uses the counter method of our timer object to 
                 count how many ticks have gone by since the last time we
                 passed our zero point on the encoder. That means that it 
                 simply tells us where we are in our encoder rotation. This 
                 method returns a numerical value which represents the motor 
                 position.
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Mar. 9, 2021
        """
        return(self.timer.counter()) #return the current position
    
    def setPosition(self, value):
        """ 
        @brief Saves the users desired position
        @details The setPosition method allows the user to input a position
                 which this method will then save as a value to be used at a 
                 later time. This value will most likely be zero, as the user 
                 will want to start their table off in a balanced state, 
                 otherwise the user will have no idea how far the system needs 
                 to go in order to become level.
        @param value An inputed value for the user, allowing them to choose 
                     where they want to be
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        """
        ##@brief Desired Value
        # @details Store the value that the user wants to be at here
        self.value = value

    def getDelta(self):
        """
        @brief Change in position
        @details Takes the two most recent values from the update method and 
                 computes the difference between the two. If the user were
                 timing the system and taking values at a specific interval,
                 this could be used to compute the speed of the motor. However,
                 the user must use the update method after this one in order to
                 change the most recent value and replace the prevPosition 
                 value with the position value, otherwise this method will 
                 return the same value over and over.
        @param None
        @author Hunter Morse
        @author Jacob Everest
        @date Mar. 9, 2021
        """
        return(self.prevPosition - self.position) #return the difference in the
        #two most recent positions
    
    def balanceDif(self):
        """
        @brief Desired value distance
        @details Uses the getPosition method to identify where the motor is
                 currently in its rotation. It then takes the difference 
                 between that value and the setPosition value that the user 
                 wanted to determine how far the motor currently is from where
                 the user wants it to be. Returns that value.
        @param None
        @author Jacob Everest
        @author Hunter Morse
        @date Mar. 9, 2021
        """
        return(self.value - self.getPosition()) #return the difference between
        #where you are and where you want to be