# -*- coding: utf-8 -*-
"""@file mainWithnFault.py
@brief An attempt at balancing the table
@details This file was simply an attempt at balancing the table on its own. To 
         do so we set up the fault pin isr when there was a problem. We then 
         created two motor driver and encoder objects to balance the table. 
         At the beginning of the while loop we ignored the fault flag for a 
         set amount of times because for some reason it kept triggering
         whenever we enabled the motors. From there we set the duty cycle based
         on distance the encoders were from their respective 0 points (where
         the table would be level). With this method the duty cycle would be 
         proportional to how far the table was from level, but within a range 
         of about 20 - 60% duty cycle. Part of this code is still there in the
         loop. It can be seen in the if statement where balanceval (where you
         want to be minus where you actually are at the moment) is between 
         -1 and -32768; duty = (((balanceval/32768)*40)-20). In this case the 
         maximum distance the encoder could be from where it wants to be was 
         -32768, so if that was was the case then the duty cycle would be -60
         (from the equation mentioned earlier). We also handled the the encoder
         being "farther" away too. Let us say that we are 32770 units away. Our
         new balance val would be -32770 (because where we want to be is zero),
         yet we because these readings exist on a circle we have actually 
         gotten closer to zero. In this case we need to account for wanting 
         our motor to turn the correct direction based on where it is. That is
         why for the next calculation of duty cycle we add balanceval to 
         65535 before dividing by 32768. By adding the positive 65535, which is
         the maximum distance on the encoder before turning back to zero, we 
         account for the fact that 32768 is truly our farthest distance. 
         Anything above it gets closer to zero as the number gets higher and 
         anything below it gets closer to zero as the number gets lower. Feel
         free to take a look at the code, we thought it was quite crafty. 
         However, one of the main problems we ran into was the friction in the
         motor arm joints. These were different to each arm and therefore made
         the input of the computation of duty cycle difficult because it 
         required different bounds for different motions. It was easier for
         both motors to pull down rather than pull up and one motor struggled
         far more than the other. Additionally, it became quite clear that even 
         though it would output a high duty cycle when pushed further and 
         further, it would never reach that far. For that reason, when the 
         displacement was low, the motors did not output enough power. That 
         was why we switched to just setting the
         duty cycle based on which side of the encoder we were reading. Like 
         before, being on the positive side or the negative side of the encoder
         mattered for which way we turned. This time though, instead of
         relating the duty to the displacement, we just gave it a set duty cycle
         no matter how far it was. The only thing that mattered was the sign
         of the duty based on the side it was on. This caused a lot of 
         vibrating because when the duty was too low, it could not push the 
         table at all when it was at anything more than a 10 degree angle from 
         flat. When it was too high it could not settle down as it kept over
         shooting. Thus, this did not work out. There was too much code on 
         one page so we changed it up and restarted in mainThatBalancedtheTable.py
@author Jacob Everest
@author Hunter Morse
@date Mar. 15, 2021
@copyright This document shall only be viewed by those who will give us an A
"""
import MotorDriver
from MotorDriver import MotorDriver
import pyb
import EncoderDriver
from EncoderDriver import EncoderDriver

##@brief Flag for faulting
# @details when high, there has been a fault, when low, it's been resolved 
faultflag = 0 #flag to be triggered when a fault occurs
##@brief Fault pin
# @details Pin that the faults are dependent upon
fault = pyb.Pin.board.PB2 #fault pin on the nucleo

def fault_isr (fault): #fault isr to be triggered when bad juju happens
    """
    @brief Fault ISR
    @details Sets the faultflag to one if a fault isr has been triggered. Stops
             all functionality until resolved.
    @author Jacob Everest
    @author Hunter Morse
    @date Mar. 15, 2021
    @copyright No
    """
    global faultflag #use fault flag in here
    faultflag = 1 #set flag to 1 to indicate a fault has occured    
##@brief External Interrupt Object
# @details Allows the Fault interrupt to be called
extint = pyb.ExtInt(pyb.Pin.board.PB2, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, fault_isr)
#external interrup to be used when current draw to PB2 is too high

##@brief Fix pin
# @Pin to be pressed that would fix the faults
fix = pyb.Pin.board.PC13 #"fix" pin, all this stuff isn't working though
def fix_isr (fix): #not working
    """
    @brief Fix ISR
    @details Was meant to allow the user to press the blue button on the 
             microcontroller to fix the fault interrupt. Didnt end up working
    @author Jacob Everest
    @author Hunter Morse
    @date Mar. 15, 2021
    @copyright No
    """
    global faultflag #not working
    faultflag = 0 #not working  
##@brief External Interrupt Object
# @details Allows the Fix interrupt to be called
fixtint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, fix_isr)
#not working

##@brief X axis motor
# @details Creates an object that controls the motor that pushes the table 
#          Around its widthwise axis
var = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB4, pyb.Pin.board.PB5, 3, 1, 2)
##@brief Y axis motor
# @details Creates an object that controls the motor that pushes the table 
#          Around its lengthwise axis
var2 = MotorDriver(pyb.Pin.board.PA15, pyb.Pin.board.PB0, pyb.Pin.board.PB1, 3, 3, 4)
#create MotorDriver object
##@brief X axis Motor encoder
# @details Encoder object that outputs position of the X axis motor
balancer = EncoderDriver(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 1, 2, pyb.Timer(4, prescaler = 7, period = 0x7fffffff))
balancer.setPosition(0)
##@brief Y axis Motor encoder
# @details Encoder object that outputs position of the Y axis motor
balancer2 = EncoderDriver(pyb.Pin.board.PC6, pyb.Pin.board.PC7, 1, 2, pyb.Timer(8, prescaler = 7, period = 0x7fffffff))
balancer2.setPosition(0)
##@brief State flag
# @details What else do you want? It's the state flag
state = 0 #use state indicator in loop later
faultflag = 0 #start out making sure fault flag is zero
##@brief Fault ignore
# @details Set number of cycles for which we will ignore the fault flag in order
#          to give the motors time to get running before a real fault
start = 300 #enabling the motor caused a fault in my system, so I created this
#to let the loop below run 300 so it can at least get started
##@brief ADC object
# @details allowed us to connect PC13 to A0 and use the blue button to end the 
#          fault state
adc = pyb.ADC(pyb.Pin.board.A0) #create ADC object so that pin A0 can read the
#value coming from the blue button on the board
##@brief Duty cycle value
# @details Percent of time X motor is receiving energy from PWM
duty = 0
##@brief Duty cycle value
# @details Percent of time Y motor is receiving energy from PWM
duty2 = 0
var.set_duty(0)
var2.set_duty(0)
var.enable()
##@brief New Duty cycle value
# @details Value used for duty cycle when proportionality became unwieldy
dut = 75
##@brief Distance to go
# @details Value computed later that represents how far we are from where we 
#          need to be for balance around the x axis
balanceval = 0
##@brief Distance to go
# @details Value computed later that represents how far we are from where we 
#          need to be for balance around the y axis
balanceval2 = 0
while True: #loop!
    if start > 0: #if motor is just starting, set faultflag to zero to allow it
    #time to get up to speed
        faultflag = 0 #set flag to zero to ignore the weird glitch
        if start == 300:
            var.enable()
            var2.enable()
            start = start - 1
            print('high')
        
        elif start == 0: #once start reaches 0, keep it there
            start = 0 #keep start equal to 0 so it doesn't try running back through
        else: #if it hasn't given the motors a chance
            start = start - 1 #decrement the start counter
    else: #if start is equal to zero, now allow the fault flag to affect the system
        pass
    if faultflag == 0: #if fault flag is not on, run normally
        if state == 0: # if motor is just starting
            var.enable() #enable the motor
            balanceval = balancer.balanceDif()
            if -32768 < balanceval < -10:
                duty = (((balanceval/32768)*40)-20) #Proportional computation!
                duty = -dut #When dut is involved, it means we have now given
                #up the proportional control and are just going with the set value
                var.set_duty(duty) 
            elif -10< balanceval < 10:
                duty = 0
                var.stop()
            else: 
                duty = ((((65536+balanceval)/32767)*40)+20) #Proportional computation
                var.set_duty(dut)
               
            balanceval2 = balancer2.balanceDif()
            if -32768 < balanceval2 < -10:
                duty2 = (((balanceval2/32768)*40)-20)
                var2.set_duty(-dut)
            elif -10< balanceval2 < 10:
                duty2 = 0
                var2.stop()
            else: 
                duty2 = ((((65536+balanceval2)/32767)*40)+20)
                var2.set_duty(dut)
               
            state = 0 #set the state to 1 so it doesnt keep re-enabling the motor
        if state > 0: #not just starting anymore #became obsolete when we kept changing the duty 
        #so we couldn't just set the duty and let it run
            pass #run
            
    else: #if fault flag has been set come here
        var.stop() #stop the motors
        var2.stop() #stop the motors
        var.disable() #disable the motors
        state = 0 #set the state back to zero so that when it starts again,
        #we re-enable the motors and reset the duty cycle
        if(adc.read() < 3000): #if the blue button has been pressed
            faultflag = 0 #allow the motors to run again
            start = 300 #set the start back to 300 to give the motors time again
        else:
            pass #otherwise, keep not running
        
    