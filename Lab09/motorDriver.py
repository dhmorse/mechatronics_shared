# -*- coding: utf-8 -*-
"""
@file motorDriver.py
@brief Motor Driver
@details Driver to interface the DC motors with the STM32 using PWM
and the DRV8847 motor driver

@author Hunter Morse and Jacob Everest
@date March 11, 2021
"""

''' @file main.py
There must be a docstring at the beginning of a Python
source file with an @file [filename] tag in it! '''
import pyb

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''
    global faultflag
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, IN1Pin_channel, IN2Pin_channel):
        ''' 
        Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
               IN1_pin and IN2_pin. 
        '''
        # self.nSLEEP_pin = nSLEEP_pin
        #self.IN1_pin = IN1_pin
        #self.IN2_pin = IN2_pin
        self.grouch = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        CW = pyb.Pin(IN1_pin, pyb.Pin.OUT_PP)
        cCW = pyb.Pin(IN2_pin, pyb.Pin.OUT_PP)
        tim = pyb.Timer(timer, freq=20000)
        self.clockwise = tim.channel(IN1Pin_channel, pyb.Timer.PWM, pin=CW)
        self.cclockwise = tim.channel(IN2Pin_channel, pyb.Timer.PWM, pin=cCW)
        print ('Creating a motor driver')

    def enable (self):
        if faultflag == 1:
            pass
        else:
            self.grouch.high()
            print ('Enabling Motor')

    def disable (self):
        self.grouch.low()
        print ('Disabling Motor')

    def set_duty (self, duty):
        ''' 
        This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor 
        '''
        if faultflag == 1:
            pass
        else:
            if duty > 0:
                self.cclockwise.pulse_width_percent(0)
                self.clockwise.pulse_width_percent(duty)
                print( "Forwards")
            elif duty < 0:
                self.clockwise.pulse_width_percent(0)
                duty = duty*(-1)
                self.cclockwise.pulse_width_percent(duty)
                print('Backwards')
            else:
                self.clockwise.pulse_width_percent(0)
                self.cclockwise.pulse_width_percent(0)
                print('Coast')
    
    def stop(self):
        if faultflag == 1:
            self.cclockwise.pulse_width_percent(50)
            self.clockwise.pulse_width_percent(50)
        else:
            pass
            

# if __name__ == '__main__':
# # Adjust the following code to write a test program for your motor class. Any
# # code within the if __name__ == '__main__' block will only run when the
# # script is executed as a standalone program. If the script is imported as
# # a module the code block will not run.

# # Create the pin objects used for interfacing with the motor driver
# pin_nSLEEP = None;
# pin_IN1 = None;
# pin_IN2 = None;

# # Create the timer object used for PWM generation
# tim = None;

# # Create a motor object passing in the pins and timer
# moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)

# # Enable the motor driver
# moe.enable()

# # Set the duty cycle to 10 percent
# moe.set_duty(10)
