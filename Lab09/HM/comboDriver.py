"""
@file comboDriver.py
@brief Lab 09
@details Combines the Encoder and Motor Drivers to successfully move motors
         to a desired position.
@author Hunter Morse
@author Jacob Everest
@date March 11, 2021
"""
import pyb
import utime
import micropython
import math

from pyb import Pin, Timer, ExtInt
from motorDriver import MotorBase, DCMotor
from encoderDriver import Encoder


class Controller:
    """
    @brief Controller class
    @details Incorperates motor and encoder driver to create a position cotrolled motor
    """
    def __init__(self, motorUnit, encoders):
        """
        @brief Controller initializer
        @details Initializes the class and saves the necessary parameters as self.variables
                 so that they may be used in different methods of the class. 
        @param motorUnit A MotorBase object consisting of 2 DC motors
        @param encoders A list of encoders associated with the the DC motors on the MotorBase
        """
        self.motorBase = motorUnit

        self.motor1 = motorUnit.motor1		# Motor objects in the MotorBase class
        self.motor2 = motorUnit.motor2

        self.encoder1 = encoders[0]			# Encoder objects in the encoders list
        self.encoder2 = encoders[1]

        self.max = self.encoder1.maxVal		# Max number of ticks read by encoder

    def goToPosition(self, motor, encoder, position):
        """
        @brief Gets the table to Position
        @details Sets appropriate motor duty to achieve desired postion and makes
                 sure we get there.
        @param motor The motor to move to a desired position
        @param encoder The encoder associated wtih the motor 
        @param position The desired position for the motor
        """
        # Note: CCW : positive, CW: Negative

        # Set desired encoder position
        encoder.setPosition(position)
        # Update encoder
        encoder.update()
        # Find difference between desired and current positions
        
        angle = encoder.getPosition() #used for the angle calculation
        angle = position - angle #the location of the angle with respect to the position we want to be
        armangle = (angle/255)*2*math.pi #angle of the motor arm, portion of rotation times 2pi
        deltaheight = 0.06*math.sin(armangle) #approx height change of the end of the arm
        tableangle = math.asin(deltaheight/0.11) #calculation of the angle of the table
        delta = encoder.getDelta() #used for the angular velocity calculation
        armdelta = (delta/255)*2*math.pi #angular speed of the motor arm
        deltavelocity = 0.06*math.sin(armdelta) #approx velocity of the lever arm
        tablevelocity = math.asin(deltavelocity/0.11) #angular velocity of the table
        Theta_dotgain = 0.05
        Theta_gain = 4
        Torque = (Theta_dotgain*tablevelocity) + (Theta_gain*tableangle)
              
        # Find #ticks and direction to move
        # if delta > halfway point go otherway
        K_t = 0.0138 #torque constant
        V_dc = 12 #voltage
        R = 2.21 #Motor resistance
        # Set motor duty cycle (min -100, max 100)
        duty = (R*100*Torque/V_dc)/K_t

        # Check Vals:
        print("Motor%d:\tcurPos: %d\tdesPos: %d\tdelta: %d\tduty: %d" \
            %(motor.id, encoder.position, encoder.prevPosition,delta, duty))

        motor.setDuty(duty)
        

    def enableMotors(self):
        """
        @brief Enables motors
        @details Enable both motors of the MotorBase by setting the sleep pin low
        @param None
        """
        self.motorBase.enableMotors()


def makeController(motorBase, encoders):
    """
    @brief Controller maker function
    @details Make a controller from a motor base and encoders
    @param motorBase MotorBase object consisting of 2 Motors
    @param encoders Tuple containing two Encoder objects
    @return controller Controller object created from input MotorBase and Encoders
    """
    # Make controller
    controller = Controller(motorBase, encoders)

    return controller




        








